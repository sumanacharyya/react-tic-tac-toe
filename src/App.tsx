import React, { useState } from "react";
import Block from "./components/Block";

import "./App.css";

function App() {
  const [state, setState] = useState(Array(9).fill(null));
  const [currentTurn, setCurrentTurn] = useState("X");

  const checkWinner = (state: any[]) => {
    const winner = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];

    for (let i = 0; i < winner.length; i++) {
      const [a, b, c] = winner[i];
      if (state[a] === state[b] && state[b] === state[c] && state[c] !== null) {
        return true;
      }
      return false;
    }
  };

  const blockClickHandler = (index: number) => {
    if (state[index] !== null) {
      return;
    }

    const stateCopy = Array.from(state);
    stateCopy[index] = currentTurn;

    setCurrentTurn(currentTurn === "X" ? "O" : "X");
    setState(stateCopy);
    const win = checkWinner(stateCopy);
    if (win) {
      alert(`${currentTurn} wins!}`);
      setState(Array(9).fill(null));
      return;
    }

    let count = 0;
    for (let i = 0; i < state.length; i++) {
      if (state[i] !== null) {
        count += 1;
      }
    }

    if (count === 8) {
      setState(Array(9).fill(null));
      alert("Draw!");
    }
  };

  return (
    <div className="board">
      <div className="row">
        <Block onClick={() => blockClickHandler(0)} value={state[0]} />
        <Block onClick={() => blockClickHandler(1)} value={state[1]} />
        <Block onClick={() => blockClickHandler(2)} value={state[2]} />
      </div>
      <div className="row">
        <Block onClick={() => blockClickHandler(3)} value={state[3]} />
        <Block onClick={() => blockClickHandler(4)} value={state[4]} />
        <Block onClick={() => blockClickHandler(5)} value={state[5]} />
      </div>
      <div className="row">
        <Block onClick={() => blockClickHandler(6)} value={state[6]} />
        <Block onClick={() => blockClickHandler(7)} value={state[7]} />
        <Block onClick={() => blockClickHandler(8)} value={state[8]} />
      </div>
    </div>
  );
}

export default App;
